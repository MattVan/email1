// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import nodemailer from 'nodemailer';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { result } = context;
    
    // PUT YOUR SAFE EMAIL RECIPIENTS HERE
    const safeRecipients = ["matt.vandinther@gmail.com", "mvandinther00@mylangara.ca"];
    
    if ( !safeRecipients.includes(result['to'])) {
      console.log( "BAD: " + result['to']);
      return context;
    }
  
    console.log( "GOOD: " + result['to']);
    
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.sendgrid.net",
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: "apikey", // SendGrid user
        pass: "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" // SendGrid password
      }
    });
    
    let info = await transporter.sendMail({
      from: '"No Reply" <noreply@494909.xyz>', // PUT YOUR DOMAIN HERE
      to: result.to, // list of receivers
      subject: "Hello " + result.id, // Subject line
      text: "Your id is: " + result.id // plain text body
    });
    
    console.log("Message sent: %s", info.messageId);
    
    return context;
  };
}
