// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import twilio from 'twilio';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { result } = context;
    const accountSid = 'ACc76b49bc606f7c931ffe9c2058250997';
    const authToken = 'b028be8e6d64bb6d3a3ad6e4480b797b';
    const client = require('twilio')(accountSid, authToken);
    
    client.messages
      .create({
         body: 'Testing Testing',
         from: '+17786539507',
         to: '+1' + result['to']
       })
    .then((message: any)=> console.log(message.sid));
    
    return context;
  };
}
